import { W4nPage } from './app.po';

describe('w4n App', () => {
  let page: W4nPage;

  beforeEach(() => {
    page = new W4nPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
