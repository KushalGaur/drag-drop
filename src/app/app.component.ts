import { Component, ElementRef, ViewChild, ChangeDetectorRef } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app works!';
  data: any;
  @ViewChild('sini') public sini: ElementRef;
  self = this;
     constructor(private cd: ChangeDetectorRef){}

  ngOnInit()
  {

    this.data="<input type='text' placeholder='Kushal' value='Heeee'/>"
    setTimeout(()=>{
      // this.sini.nativeElement.innerHTML = "<app-line-chart></app-line-chart>"
      // this.cd.detectChanges();
      this.data=true;
    }, 100)
    this.cd.detectChanges();
    //this.sini.nativeElement.innerHTML = this.data;
  }
}
